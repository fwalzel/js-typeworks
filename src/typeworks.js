(function () {
    var Typeworks = /** @class */ (function () {
        function Typeworks() {
            this.targets = document.querySelectorAll('[data-prompt]');
        }
        /**
         *
         * @param which
         */
        Typeworks.prototype.start = function (which) {
            if (typeof which === 'undefined' || which == null) {
                console.log('starting all instances');
                console.log(this.targets);
            }
            else if (typeof which === 'string') {
                console.log('starting instances by selector');
            }
            else if (typeof which === 'object') {
                console.log('starting mixed instances');
            }
            else {
                console.log('@Typeworks: Wrong argument at declaration');
                return;
            }
        };
        return Typeworks;
    }());
    if (typeof window.Typeworks === 'undefined')
        window.Typeworks = Typeworks;
    else
        console.log('@Typeworks: Class could not be constructed, a variable with that name already exists in window object.');
})();
