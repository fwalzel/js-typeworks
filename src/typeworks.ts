(function() {

    class Typeworks {
        targets:any;
        constructor() {
            this.targets = document.querySelectorAll('[data-prompt]');
        }

        /**
         *
         * @param which
         */
        start(which) {

            if (typeof which === 'undefined' || which == null) {
                console.log('starting all instances');
                console.log(this.targets);
            }
            else if (typeof which === 'string') {
                console.log('starting instances by selector');
            }
            else if (typeof which === 'object') {
                console.log('starting mixed instances');
            }
            else {
                console.log('@Typeworks: Wrong argument at declaration');
                return;
            }
        }






    }

    if (typeof (<any>window).Typeworks === 'undefined')
        (<any>window).Typeworks = Typeworks;
    else
        console.log('@Typeworks: Class could not be constructed, a variable with that name already exists in window object.');

})();